import base64
import humanize
from PIL import Image
from flask import Flask, redirect, url_for, request, render_template, flash
from werkzeug.utils import secure_filename
from models import Images
from db import db_init, db

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///img.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = 'this is soooo hard'
app.config['UPLOAD_EXTENSIONS'] = ['jpg','jpeg', 'png', 'gif']
db_init(app)


@app.route('/')
def index():
    '''display a table include thumbnail and images data that user uploaded'''
    pics = Images.query.all()
    if pics:
        all_pics = pics
        if request.method == 'POST':
            return redirect(url_for('upload'))
        return render_template('index.html',all_pic=all_pics)
    return render_template('index.html')


def render_picture(data):
    render_pic = base64.b64encode(data).decode('ascii') 
    return render_pic


@app.route('/upload', methods=['POST'])
def upload():
    """ First of all here we check if the form is not empty.
    - after that we secure the name of file that user sents to avoid attackes.
    - check if file format is valid (images only allowed)
    - store image into the database in binary format, blob
    - assigning file name, dimensions , Size , Format , upload date
    - after user submit, app redirects to /upload and shows file data
    """
    uploaded_file = request.files['file']
    if not uploaded_file:
        return redirect(url_for('index'))
    uploaded_filename = secure_filename(uploaded_file.filename)
    file_type = uploaded_file.filename[-5:].split('.')[1]
    if file_type not in app.config['UPLOAD_EXTENSIONS']:
        flash('This format is not allowed. please choose an image!')
        return redirect(url_for('index'))
    data = uploaded_file.read()
    render_file = render_picture(data)
    img_size = Image.open(uploaded_file)
    size = img_size.size
    siz = '%s * %s ' % (size[0],size[1])
    vol = str(len(img_size.fp.read()))
    img_vol = humanize.naturalsize(vol)
    img = Images(name=uploaded_filename,rendered_data=render_file,
                data=data,dimensions=siz, size=img_vol)
    db.session.add(img)
    db.session.commit()
    file_date = img.pic_date
    file_render = img.rendered_data
    file_id = img.id
    return render_template('upload.html',file_name=uploaded_filename,siz=siz,
                            file_date=file_date,file_render=file_render,
                            file_id=file_id,file_type=file_type,img_vol=img_vol)


@app.route('/<int:id>')
def pic(id):
    get_pic = Images.query.filter_by(id=id).first()
    return render_template('pic.html', pic=get_pic)


@app.route('/<int:id>/delete', methods=['GET', 'POST'])
def delete(id):
    del_pic = Images.query.get(id)
    if request.method == 'POST':
        form = request.form['delete']
        if form == 'Delete':
            db.session.delete(del_pic)
            db.session.commit()
            return redirect(url_for('index'))
    return redirect(url_for('index'))