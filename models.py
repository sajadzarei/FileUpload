from db import db
from datetime import datetime


class Images(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    rendered_data = db.Column(db.Text)
    name = db.Column(db.String(128))
    data = db.Column(db.LargeBinary, nullable=False)
    pic_date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    dimensions = db.Column(db.String(80))
    size = db.Column(db.String(80))
    
    def __repr__(self):
        return f'Pic Name: {self.name} Data: {self.data} created on: {self.pic_date} Size: {self.size}'