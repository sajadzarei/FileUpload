from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
#create database
# when app runs. the img.db will appear in app's top level directory
def db_init(app):
    db.init_app(app)
    with app.app_context():
        db.create_all()