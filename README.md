# FileUpload
User can upload files in app. (images allowed only  [gif, png, jpg])
and the app shows a list of files include file's data and thumbnail: date, size, name,etc.

![Screenshot_20210523_161549](https://user-images.githubusercontent.com/71011395/119261942-bcdad280-bbee-11eb-8e88-190816b50bce.png)

## used:
- python3 | FLASK framework
- SQLite database

## to use this app:
- install python3, pip3, virtualenv
- clone the project 
- create a virtualenv named venv using ``` virtualenv -p python3 venv```
- Connect to virtualenv using ``` source venv/bin/activate ```
- from the project folder, install packages using ``` pip install -r requirements.txt ```
- finally ```$ flask run```
